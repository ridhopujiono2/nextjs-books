export const API_URL = "http://localhost:3000";

export const PATH = {
  home: "/",
  books: "/books",
  login: "/auth/login",
  register: "/auth/register",
  create_book: "/books/create",
};
