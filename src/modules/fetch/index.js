import { instance } from "../axios";

// Function for register user endpoint
async function registerUser(name, email, password) {
  try {
    const response = await instance.post("/api/register", {
      name: name,
      email: email,
      password: password,
    });
    return response.data;
  } catch (error) {
    throw new Error(error.response.data.data || "Something went wrong");
  }
}

// Function for login user endpoint
async function loginUser(email, password) {
  try {
    const response = await instance.post("/api/login", {
      email: email,
      password: password,
    });
    return response.data;
  } catch (error) {
    throw new Error(error.response.data.message || "Something went wrong");
  }
}

// Function for create book endpoint
async function createBook(formData) {
  try {
    const response = await instance.post("/api/books", formData, {
      headers: { "Content-Type": "multipart/form-data" },
    });
    console.log(response.data);
    return response.data;
  } catch (error) {
    throw new Error(error.response.data.message || "Something went wrong");
  }
}

// Function for get all books endpoint
async function getAllBooks() {
  try {
    const response = await instance.get("/api/books");
    return response.data;
  } catch (error) {
    throw new Error(error.response.data.message || "Something went wrong");
  }
}

// Function for edit book endpoint
async function editBook(id, title, author, publisher, year, pages) {
  try {
    const response = await instance.put(`/api/books/${id}`, {
      title,
      author,
      publisher,
      year,
      pages,
    });
    return response.data;
  } catch (error) {
    throw new Error(error.response.data.message || "Something went wrong");
  }
}

// Function for delete book endpoint
async function deleteBook(id) {
  try {
    const response = await instance.delete(`/api/books/${id}`);
    return response.data;
  } catch (error) {
    throw new Error(error.response.data.message || "Something went wrong");
  }
}

async function getBookDetailById(id) {
  try {
    const response = await instance.get(`/api/books/${id}`);
    return response.data;
  } catch (error) {
    throw new Error(error.response.data.message || "Something went wrong");
  }
}

export {
  registerUser,
  loginUser,
  createBook,
  getAllBooks,
  editBook,
  deleteBook,
  getBookDetailById,
};
