import React, { useEffect, useState } from "react";
import Button from "./Button";
import { BookOpen } from "react-feather";
import Link  from "next/link";
import { useRouter } from "next/router";
import toast, { Toaster } from "react-hot-toast";
import { PATH } from "@/modules/path/path";
import { useGlobalContext } from "@/context/globalContext";

function Navbar() {
  const {isLogin,setIsLogin} = useGlobalContext();

  return (
    <div>
      <nav className="flex justify-between items-center bg-slate-100 w-100 h-50 py-4 px-7 text-slate-800">
        <div>
          <Button>
            <BookOpen />
          </Button>
        </div>
        <div>
          <ul className="inline-flex gap-10">
            <li>
              <Link
                href={PATH.home}
              >
                Home
              </Link>
            </li>
            <li>
              <Link
                href={PATH.books}
              >
                Books
              </Link>
            </li>
            {isLogin && (
              <>
                <li>
                  <Link
                    href={PATH.create_book}
                  >
                    <span className="bg-yellow-300 py-1 px-2 rounded">
                      Publish book
                    </span>
                  </Link>
                </li>
              </>
            )}
          </ul>
        </div>
        <div>
          {!isLogin ? (
            <>
              <Link href={PATH.login}>
                  <span className="text-emerald-500 mr-3">Login</span>
              </Link>
              |
              <Link href={PATH.register}>
              <span className="ml-3">Register</span>
              </Link>
            </>
          ) : (
            <>
              <button onClick={() => {
                window.localStorage.removeItem("token");
                setIsLogin(false);
                toast.success("You have successfully logged out.");
              }}
              className="ml-3 text-red-500">
                Logout
              </button>
            </>
          )}
        </div>
      </nav>    
      <Toaster />
    </div>
  );
}

export default Navbar;