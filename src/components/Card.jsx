import React from "react";
import Link from "next/link";
import { PATH } from "@/modules/path/path";
import Image from "next/image";

function Card(props) {
  let img_src = props.image.replace(/\\/g, "/")
  return (
    <div className="bg-white rounded-lg border shadow-sm overflow-hidden w-full sm:w-1/2 md:w-1/5 mb-4">
      <Link href={`${PATH.books}/${props.id}`}>
        <Image
          className="object-cover w-full h-48"
          src={img_src}
          width={500}
          height={500}
          alt="Product"
        />
        <div className="p-4">
          <h2 className="text-lg font-medium text-gray-800 mb-2">
            {props.title}
          </h2>
          <p className="text-sm text-gray-700 mb-4">{props.author}</p>
        </div>
      </Link>
    </div>
  );
}

export default Card;
