import React from "react";
import  Link  from "next/link";
import { PATH } from "@/modules/path/path";
import Button from "./Button";
import Image from 'next/image'

function Hero() {
  return (
    <div className="flex justify-between items-center h-screen bg-slate-100 w-screen p-20">
      <div className="flex-1">
        <h1 className="text-4xl leading-normal tracking-wide text-slate-700 mb-5">
          Are you looking for books that are trending right now?
        </h1>
        <Link
          href={PATH.books}
          className="bg-emerald-500 text-white rounded border px-4 py-3"
        >
          Get Started !
        </Link>
      </div>
      <div className="flex-1">
      <Image
        src={"/hero.png"}
        alt="Picture of the author"
        width={500}
        height={500}
      />
      </div>
    </div>
  );
}

export default Hero;
