import Navbar from "./Navbar";

function Container(props) {
  return (
   <div className="h-screen">
      <Navbar />
      {props.children}
   </div>
  );
}

export default Container;
