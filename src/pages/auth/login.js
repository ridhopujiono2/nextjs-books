import React, { useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import Button from "@/components/Button";
import { PATH } from "@/modules/path/path";
import toast, { Toaster } from "react-hot-toast";
import { Loader } from "react-feather";
import { loginUser } from "@/modules/fetch";
// import { sleep } from "../../utils/sleep";

function Login() {
  const [disabled, setDisabled] = useState(true);
  const [loading, setLoading] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const router = useRouter();
  const submitHandler = async (e) => {
    e.preventDefault();
    try {
      const auth = await loginUser(email, password);
      if (auth.token) {
        window.localStorage.setItem("token", auth.token);
        toast.success("Login Successfully", {
          duration: 4000,
        });
        window.location.href = PATH.home;
      }
    } catch (err) {
      toast.error(err.message, {
        duration: 4000,
      });
    }
  };

  return (
    <div className="flex h-screen items-center justify-center">
      <div className="border w-1/3 p-10 rounded">
        <h1 className="text-2xl mb-5">Login</h1>

        <form
          onSubmit={(e) => {
            submitHandler(e);
          }}
        >
          <div className="mb-3">
            <input
              type="email"
              className="py-2 px-4 w-full border rounded"
              placeholder="Email"
              onChange={(e) => {
                setEmail(e.target.value);
              }}
              value={email}
              required
              name="email"
            />
          </div>
          <div className="mb-3">
            <input
              type="password"
              className="py-2 px-4 w-full border rounded"
              placeholder="Password"
              onChange={(e) => {
                setPassword(e.target.value);
              }}
              value={password}
              required
              name="password"
            />
          </div>
          <div className="flex justify-between">
            <Link className="text-emerald-800" href={PATH.register}>
              Create account ?
            </Link>
            <Button>Login</Button>
          </div>
        </form>
      </div>
      <Toaster />
    </div>
  );
}

export default Login;
