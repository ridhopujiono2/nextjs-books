import React, { useEffect, useState } from "react";
import Card from "@/components/Card";
import { getAllBooks } from "@/modules/fetch";
import Image from "next/image";
import Container from "@/components/Container";

function BooksPage() {
  const [books, setBooks] = useState([]);

  useEffect(() => {
    const fetchBooks = async () => {
      const { books } = await getAllBooks();
      setBooks(books);
    };

    fetchBooks();
  }, []);
  return (
    <Container>
      <div className="p-10">
        <div className="flex flex-wrap justify-evenly gap-2">
          {books?.map((book, idx) => (
            <Card key={idx} {...book} />
          ))}
        </div>
      </div>
    </Container>
  );
}

export default BooksPage;
